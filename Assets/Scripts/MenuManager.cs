﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

	public void NewGame_OnClick()
	{
		GameObject.FindObjectOfType<AudioSource>().Play();
		UnityEngine.SceneManagement.SceneManager.LoadScene(1);		
	}
	public void QuitGame_OnClick()
	{
		GameObject.FindObjectOfType<AudioSource>().Play();
		Application.Quit();
	}
}