﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
	float period = 0.00f;
	GameManager gameManager;	

	void Start()
	{
		gameManager = FindObjectOfType<GameManager>();
		gameManager.SetActiveBlock(this);

		// Game over if default position is invalid
		if (!IsValidGridPos())
		{
			Destroy(gameObject);
			gameManager.GameOver();
		}
	}

	public void MoveLeft()
	{
		transform.position += new Vector3(-1, 0, 0);
		if (IsValidGridPos())
		{
			UpdateGrid();
		}
		else
		{			
			transform.position += new Vector3(1, 0, 0);  //Revert
		}
	}
	public void MoveRight()
	{
		transform.position += new Vector3(1, 0, 0);
		if (IsValidGridPos())
		{
			UpdateGrid();
		}
		else
		{
			//Revert
			transform.position += new Vector3(-1, 0, 0);
		}
	}

	public void Rotate()
	{
		transform.Rotate(0, 0, -90);

		if (IsValidGridPos())
		{
			UpdateGrid();
		}
		else
		{
			//Revert
			transform.Rotate(0, 0, 90);
		}
	}

	void Update()
	{
		if (!gameManager.IsGamePaused())
		{
			if (period > (4 / ((float)gameManager.level + 5)))
			{
				period = 0;
				Fall();
				period += Time.deltaTime;
				return;
			}
			period += Time.deltaTime;
		}
	}

	public void Fall()
	{
		if (this == null)
		{
			return; //Prevents bug due to blocks being deleted faster than the fall method executes
		}
		transform.position += new Vector3(0, -1, 0);

		if (IsValidGridPos())
		{
			UpdateGrid();
		}
		else
		{
			//Revert
			transform.position += new Vector3(0, 1, 0);

			// Clear filled horizontal lines
			gameManager.DeleteFullRows();

			gameManager.sfxAudioSource.clip = gameManager.audioBlockImpacts[Random.Range(0, gameManager.audioBlockImpacts.Length)];
			gameManager.sfxAudioSource.Play();

			// Spawn next Group
			gameManager.blockSpawnCount += 1;
			FindObjectOfType<Spawner>().SpawnNext();

			//Increase score
			gameManager.IncreaseScore(1);						

			// Disable script
			enabled = false;
		}
	}

	public bool IsValidGridPos()
	{
		//Verify child block positions
		foreach (Transform child in transform)
		{
			Vector2 v = gameManager.RoundVec2(child.position);

			// Not inside Border?
			if (!gameManager.InsideBorder(v))
			{
				return false;
			}

			// Block in grid cell (and not part of same group)?
			if (gameManager != null && gameManager.grid[(int)v.x, (int)v.y] != null && gameManager.grid[(int)v.x, (int)v.y].parent != transform)
			{
				return false;
			}
		}
		return true;
	}

	void UpdateGrid()
	{
		// Remove old children from grid
		for (int y = 0; y < gameManager.h; y++)
		{
			for (int x = 0; x < gameManager.w; x++)
			{
				if (gameManager.grid[x, y] != null)
				{
					//Does the block's parent equal the current group's transform?
					if (gameManager.grid[x, y].parent == transform)
					{
						//It is a child of the group, remove it
						gameManager.grid[x, y] = null;
					}
				}
			}
		}

		// Add new children to grid
		foreach (Transform child in transform)
		{
			Vector2 v = gameManager.RoundVec2(child.position);
			gameManager.grid[(int)v.x, (int)v.y] = child;
		}
	}
}