﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	[Range(0, 100)]
	public int level = 0;
	private int score;
	public int blockSpawnCount;
	public int levelIncrementRate = 5;
	public int rowsClearedCount;
	public GameObject scoreText;
	public GameObject levelText;
	public GameObject gameOverPanel;
	public GameObject pauseMenuPanel;

	public AudioClip[] audioBlockImpacts;
	public AudioClip[] audioLevelUp;	
	public AudioSource sfxAudioSource;
	public AudioSource levelUpAudioSource;
	private Spawner spawner;

	public int w = 10;
	public int h = 24;
	public Transform[,] grid;
	private Block activeBlock;
	private int leftArrowKeyTimePreviouslyPressed, rightArrowKeyTimePreviouslyPressed, upArrowKeyTimePreviouslyPressed;
	private float leftArrowKeyDownStartTime, rightArrowKeyDownStartTime, upArrowKeyDownStartTime;
	private bool upArrowButtonHeld, rightArrowButtonHeld, downArrowButtonHeld, leftArrowButtonHeld;
	private bool gamePaused;	

	public bool IsGamePaused()
	{
		return gamePaused;
	}

	public void SetGamePaused(bool setTo)
	{
		gamePaused = setTo;
	}

	public void SetActiveBlock(Block b)
	{
		activeBlock = b;
	}
	public Block GetActiveBlock()
	{
		return activeBlock;
	}

	void Start()
	{
		grid = new Transform[w, h];
		gameOverPanel.SetActive(false);
		pauseMenuPanel.SetActive(false);
		spawner = GameObject.FindObjectOfType<Spawner>();
	}
	void Update()
	{
		scoreText.GetComponent<Text>().text = "SCORE " + score.ToString();
		levelText.GetComponent<Text>().text = "LEVEL " + level.ToString();

		//ESCAPE
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SetGamePaused(!IsGamePaused());
			pauseMenuPanel.SetActive(IsGamePaused());
		}
		if (IsGamePaused())
		{
			return;
		}

		//DOWN
		//if (Input.GetKeyDown(KeyCode.DownArrow))
		//{
		//	if (!IsGamePaused() && activeBlock != null)
		//	{
		//		activeBlock.SetDownArrowHeld(true);
				
		//	}
		//}
		//else if (Input.GetKeyUp(KeyCode.DownArrow))
		//{
		//	if (!IsGamePaused() && activeBlock != null)
		//	{
		//		activeBlock.SetDownArrowHeld(false);
		//	}
		//}
		if (Input.GetKey(KeyCode.DownArrow) || downArrowButtonHeld)
		{
			if (!IsGamePaused())
			{
				activeBlock.Fall();
			}
		}

		//LEFT
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			leftArrowKeyDownStartTime = Time.time + 1;
			//gameManager.OnLeftArrow();
		}
		else if (Input.GetKeyUp(KeyCode.LeftArrow))
		{
			leftArrowKeyDownStartTime = 0;
		}
		if (Input.GetKey(KeyCode.LeftArrow) || leftArrowButtonHeld)
		{
			int timePressed = (int)((Time.time - leftArrowKeyDownStartTime) * 6);
			if (timePressed > leftArrowKeyTimePreviouslyPressed)
			{
				OnLeftArrow();
			}
			leftArrowKeyTimePreviouslyPressed = timePressed;
		}

		//RIGHT
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			//gameManager.OnRightArrow();
			rightArrowKeyDownStartTime = Time.time + 1;
		}
		else if (Input.GetKeyUp(KeyCode.RightArrow))
		{
			rightArrowKeyDownStartTime = 0;
		}
		if (Input.GetKey(KeyCode.RightArrow) || rightArrowButtonHeld)
		{
			int timePressed = (int)((Time.time - rightArrowKeyDownStartTime) * 6);
			if (timePressed > rightArrowKeyTimePreviouslyPressed)
			{
				OnRightArrow();
			}
			rightArrowKeyTimePreviouslyPressed = timePressed;
		}
		
		//UP
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			upArrowKeyDownStartTime = Time.time + 1;
		}
		else if (Input.GetKeyUp(KeyCode.UpArrow))
		{
			upArrowKeyDownStartTime = 0;
		}
		if (Input.GetKey(KeyCode.UpArrow) || upArrowButtonHeld)
		{
			int timePressed = (int)((Time.time - upArrowKeyDownStartTime) * 3);
			if (timePressed > upArrowKeyTimePreviouslyPressed)
			{
				OnUpArrow();
			}
			upArrowKeyTimePreviouslyPressed = timePressed;
		}
	
	}
	
	public void GameOver()
	{
		gameOverPanel.SetActive(true);
	}

	public void ClearGrid()
	{
		Block[] gameObjects = GameObject.FindObjectsOfType<Block>();

		for (var i = 0; i < gameObjects.Length; i++)
		{
			Destroy(gameObjects[i].gameObject);
		}

		//Start the game again
		grid = new Transform[w, h];
	}

	public int GetScore()
	{
		return score;
	}

	public void IncreaseScore(int inc)
	{
		score += inc;
	}

	public void SetLevel(int lvl)
	{
		if (lvl != level)
		{			
			levelUpAudioSource.clip = audioLevelUp[Random.Range(0, audioLevelUp.Length)];
			levelUpAudioSource.Play();
		}
		level = lvl;
	}

	public Vector2 RoundVec2(Vector2 v)
	{
		//Prevents inaccuracies with rotations
		return new Vector2(Mathf.Round(v.x), Mathf.Round(v.y));
	}

	public bool InsideBorder(Vector2 pos)
	{
		return ((int)pos.x >= 0 && (int)pos.x < w && (int)pos.y >= 0); //&& (int)pos.y <h
	}

	public void DeleteRow(int y)
	{		
		for (int x = 0; x < w; ++x)
		{
			Destroy(grid[x, y].gameObject);
			grid[x, y] = null;
		}
	}

	public void DecreaseRow(int y)
	{
		for (int x = 0; x < w; ++x)
		{
			if (grid[x, y] != null)
			{
				// Move grid info down and delete old info
				grid[x, y - 1] = grid[x, y];
				grid[x, y] = null;

				// Update Block position by moving it down
				grid[x, y - 1].position += new Vector3(0, -1, 0);
			}
		}
	}

	public void DecreaseRowsAbove(int y)
	{
		for (int i = y; i < h; ++i)
		{
			DecreaseRow(i);
		}
	}

	public bool IsRowFull(int y)
	{
		for (int x = 0; x < w; ++x)
		{
			if (grid[x, y] == null)
			{
				return false;
			}
		}
		return true;
	}

	public void DeleteFullRows()
	{
		int rowRemovalCount = 0;
		for (int y = 0; y < h; ++y)
		{
			if (IsRowFull(y))
			{				
				DeleteRow(y);
				DecreaseRowsAbove(y + 1);
				y--;
				rowRemovalCount++;
			}
		}

		if (rowRemovalCount > 0)
		{
			rowsClearedCount += rowRemovalCount;
			SetLevel(rowsClearedCount / 10);
			IncreaseScore(2 * rowRemovalCount * level);
		}
	}

	public void OnUpArrow()
	{
		if (!IsGamePaused() && activeBlock != null && activeBlock.IsValidGridPos())
		{
			activeBlock.Rotate();
		}
	}

	public void OnLeftArrow()
	{
		if (!IsGamePaused() && activeBlock != null)
		{
			activeBlock.MoveLeft();
		}
	}

	public void OnRightArrow()
	{
		if (!IsGamePaused() && activeBlock != null)
		{
			activeBlock.MoveRight();
		}
	}

	public void Resume_Click()
	{
		pauseMenuPanel.SetActive(false);
		gamePaused = false;
	}

	public void TryAgain_Click()
	{
		score = 0;
		level = 0;
		rowsClearedCount = 0;
		ClearGrid();
		gameOverPanel.SetActive(false);
		blockSpawnCount = 1;
		spawner.nextBlockIndex = Random.Range(0, spawner.blocks.Length);
		spawner.SpawnNext();
	}

	public void QuitGame_Click()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}

	public void Pause_Click()
	{
		gamePaused = true;
		pauseMenuPanel.SetActive(true);
	}
	
	public void UpPointerDown()
	{
		upArrowKeyDownStartTime = Time.time + 1;		
		upArrowButtonHeld = true;
	}	
	public void UpPointerUp()
	{
		upArrowKeyDownStartTime = 0;
		upArrowButtonHeld = false;
	}

	public void RightPointerDown()
	{
		rightArrowKeyDownStartTime = Time.time + 1;
		rightArrowButtonHeld = true;
	}
	public void RightPointerUp()
	{
		rightArrowKeyDownStartTime = 0;
		rightArrowButtonHeld = false;
	}

	public void DownPointerDown()
	{		
		downArrowButtonHeld = true;
	}
	public void DownPointerUp()
	{		
		downArrowButtonHeld = false;
	}

	public void LeftPointerDown()
	{
		leftArrowKeyDownStartTime = Time.time + 1;
		leftArrowButtonHeld = true;
	}
	public void LeftPointerUp()
	{
		leftArrowKeyDownStartTime = 0;
		leftArrowButtonHeld = false;
	}

	
}