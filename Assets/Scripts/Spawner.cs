﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
	public GameObject[] blocks;
	public GameObject[] previewBlocks;
	private GameObject previewBlock;
	public int nextBlockIndex;
	public Transform nextWindowTransform;
	
	private void Start()
	{
		GameObject.FindObjectOfType<GameManager>().blockSpawnCount = 1;
		SpawnNext();
	}

	public void SpawnNext()
	{
		
		//Use default rotation to spawn
		Instantiate(blocks[nextBlockIndex], transform.position, Quaternion.identity);
		nextBlockIndex = Random.Range(0, blocks.Length);
		if (previewBlock != null)
		{
			GameObject.Destroy(previewBlock);
		}
		Vector3 v3 = new Vector3(nextWindowTransform.position.x, nextWindowTransform.position.y + 0.25f);
		previewBlock = Instantiate(previewBlocks[nextBlockIndex], v3, Quaternion.identity);
	}
}
